
# README #

### Prop�sito ###
Solu��o desenvolvida para teste de sele��o do Walmart para vaga de desenvolvedor Java de back-end

### Configura��o do ambiente ###
* Configuration
NetBeans IDE 8.0.2
Java DB
Servidor de aplica��o GlassFish Server 4.1

* Depend�ncias
N�o h� outras depend�ncias

* Banco de dados
Banco utilizado: Java DB
Nome do banco de dados: WalmartTestDB
Usu�rio/senha: walmart/walmart
Scripts de cria��o de tabelas:
CREATE TABLE CITY (ID CHAR(2) NOT NULL, NAME VARCHAR(50) NOT NULL, PRIMARY KEY (ID));
CREATE TABLE ROAD (ID INTEGER NOT NULL, ORIGINID CHAR(2) NOT NULL, DESTINATIONID CHAR(2) NOT NULL, DISTANCE DOUBLE NOT NULL, PRIMARY KEY (ID), FOREIGN KEY (DESTINATIONID) REFERENCES CITY (ID), FOREIGN KEY (ORIGINID) REFERENCES CITY (ID));

* Testes unit�rios
Os test cases gerados est�o no package com.walmart.routefinder.test

### Descri��o da Solu��o ###
* Web services desenvolvidos
1. City: fun��es de CRUD para cria��o, leitura, edi��o e dele��o de cidades. Foi dado foco �s fun��es de cria��o e leitura
Exemplo de JSON para cria��o de Cidade: { "id" : "A", "name" : "City A" }

2. Road: fun��es de CRUD para cria��o, leitura, edi��o e dele��o de estradas. Foi dado foco �s fun��es de cria��o e leitura
Exemplo de JSON para cria��o de Estrada: {"destinationid":{"id":"B","name":"City B"}, "distance":10.0, "id":0, "originid":{"id":"A","name":"City A"}}

3. Route: Consulta para localiza��o de melhor rota entre duas cidades e custo de combust�vel resultante do deslocamento na rota
Exemplo de consulta de rota: findBestRoute(String origemId, String destinoId, double valorCombustivel, double autonomia)
Exemplo de resposta: "City A - City B; City B - City D; fuel cost = 6.25"

* Descri��o do algoritmo usado
Cidades e estradas foram representadas respectivamente como n�s e arcos de um grafo que representa o mapa alimentado no sistema. 
Cada vez que uma cidade ou estrada � criada o mapa � atualizado para otimizar o tempo de consulta de rota, 
eliminando o esfor�o de criar todo o mapa em mem�ria a cada consulta. Para isto o Mapa foi implementado utilizando o padr�o Singleton (ver classe Mapgraph). 
O m�todo de consulta de melhor rota (pelo crit�rio adotado de melhor rota = rota mais curta) � uma adapta��o do m�todo 
DFS (Depth-First Search) de varredura de grafos, e possui complexidade semi-linear, O(N * log (N)), onde N = n�mero de cidades no mapa.


