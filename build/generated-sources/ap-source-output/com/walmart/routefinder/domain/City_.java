package com.walmart.routefinder.domain;

import com.walmart.routefinder.domain.Road;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-03-07T15:41:48")
@StaticMetamodel(City.class)
public class City_ { 

    public static volatile CollectionAttribute<City, Road> adjacents;
    public static volatile CollectionAttribute<City, Road> destinations;
    public static volatile SingularAttribute<City, String> name;
    public static volatile CollectionAttribute<City, Road> origins;
    public static volatile SingularAttribute<City, String> id;

}