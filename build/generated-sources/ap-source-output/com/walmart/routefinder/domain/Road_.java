package com.walmart.routefinder.domain;

import com.walmart.routefinder.domain.City;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-03-07T15:41:48")
@StaticMetamodel(Road.class)
public class Road_ { 

    public static volatile SingularAttribute<Road, City> originid;
    public static volatile SingularAttribute<Road, Double> distance;
    public static volatile SingularAttribute<Road, Integer> id;
    public static volatile SingularAttribute<Road, City> destinationid;

}