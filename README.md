# README #

### Propósito ###
Solução desenvolvida para teste de seleção do Walmart para vaga de desenvolvedor Java de back-end

### Configuração do ambiente ###
* Configuration
NetBeans IDE 8.0.2
Java DB
Servidor de aplicação GlassFish Server 4.1

* Dependências
Não há outras dependências

* Banco de dados
Banco utilizado: Java DB
Nome do banco de dados: WalmartTestDB
Usuário/senha: walmart/walmart
Scripts de criação de tabelas:
CREATE TABLE CITY (ID CHAR(2) NOT NULL, NAME VARCHAR(50) NOT NULL, PRIMARY KEY (ID));
CREATE TABLE ROAD (ID INTEGER NOT NULL, ORIGINID CHAR(2) NOT NULL, DESTINATIONID CHAR(2) NOT NULL, DISTANCE DOUBLE NOT NULL, PRIMARY KEY (ID), FOREIGN KEY (DESTINATIONID) REFERENCES CITY (ID), FOREIGN KEY (ORIGINID) REFERENCES CITY (ID));

* Testes unitários
Os test cases gerados estão no package com.walmart.routefinder.test

### Descrição da Solução ###
* Web services desenvolvidos
1. City: funções de CRUD para criação, leitura, edição e deleção de cidades. Foi dado foco às funções de criação e leitura
Exemplo de JSON para criação de Cidade: { "id" : "A", "name" : "City A" }

2. Road: funções de CRUD para criação, leitura, edição e deleção de estradas. Foi dado foco às funções de criação e leitura
Exemplo de JSON para criação de Estrada: {"destinationid":{"id":"B","name":"City B"}, "distance":10.0, "id":0, "originid":{"id":"A","name":"City A"}}

3. Route: Consulta para localização de melhor rota entre duas cidades e custo de combustível resultante do deslocamento na rota
Exemplo de consulta de rota: findBestRoute(String origemId, String destinoId, double valorCombustivel, double autonomia)

* Descrição do algoritmo usado
Cidades e estradas foram representadas respectivamente como nós e arcos de um grafo que representa o mapa alimentado no sistema. Cada vez que uma cidade ou estrada é criada o mapa é atualizado para otimizar o tempo de consulta de rota, eliminando o esforço de criar todo o mapa em memória a cada consulta. Para isto o Mapa foi implementado utilizando o padrão Singleton (ver classe Mapgraph). O método de consulta de melhor rota (pelo critério adotado de melhor rota = rota mais curta) é uma adaptação do método DFS (Depth-First Search) de varredura de grafos, e possui complexidade semi-linear, O(N * log (N)), onde N = número de cidades no mapa.
