/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.routefinder.test;

import com.walmart.routefinder.domain.City;
import com.walmart.routefinder.domain.MapGraph;
import com.walmart.routefinder.domain.Road;
import java.util.Iterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Artur
 */
public class MapGraphTest {
    private static City A = new City("A", "City A");
    private static City B = new City("B", "City B");
    private static City C = new City("C", "City C");
    private static City D = new City("D", "City D");
    private static City E = new City("E", "City E");
    private static City F = new City("F", "City F");
    private static Road AB = new Road(1, 10.0, A, B);
    private static Road AC = new Road(2, 40.0, A, C);
    private static Road BD = new Road(3, 60.0, B, D);
    private static Road CD = new Road(4, 30.0, C, D);
    private static Road BC = new Road(5, 20.0, B, C);
    private static City A1 = new City("A1", "City A1");
    private static City B1 = new City("B1", "City B1");
    private static City C1 = new City("C1", "City C1");
    private static City D1 = new City("D1", "City D1");
    private static City E1 = new City("E1", "City E1");
    private static City F1 = new City("F1", "City F1");
    private static Road A1B1 = new Road(1, 5.0, A1, B1);
    private static Road A1C1 = new Road(2, 25.0, A1, C1);
    private static Road A1D1 = new Road(3, 20.0, A1, D1);
    private static Road A1F1 = new Road(4, 50.0, A1, F1);
    private static Road B1E1 = new Road(5, 35.0, B1, E1);
    private static Road B1F1 = new Road(6, 20.0, B1, F1);
    private static Road D1E1 = new Road(8, 5.0, D1, E1);
    private static Road E1F1 = new Road(10, 30.0, E1, F1);
    
    public MapGraphTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        A.addAdjacent(AB);
        A.addAdjacent(AC);
        B.addAdjacent(BC);
        B.addAdjacent(BD);
        C.addAdjacent(CD);
        A1.addAdjacent(A1B1);
        A1.addAdjacent(A1C1);
        A1.addAdjacent(A1D1);
        A1.addAdjacent(A1F1);
        B1.addAdjacent(B1F1);
        B1.addAdjacent(B1E1);
        D1.addAdjacent(D1E1);
        E1.addAdjacent(E1F1);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class MapGraph.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        String expResult = "class com.walmart.routefinder.domain.MapGraph";
        MapGraph result = MapGraph.getInstance();
        assertEquals(expResult, result.getClass().toString());
    }

    /**
     * Test of getIsDirected method, of class MapGraph.
     */
    @Test
    public void testGetIsDirected() {
        System.out.println("getIsDirected");
        boolean expResult = false;
        boolean result = MapGraph.getInstance().getIsDirected();
        assertEquals(expResult, result);
    }

    /**
     * Test of addCity method, of class MapGraph.
     */
    @Test
    public void testAddCity() {
        System.out.println("addCity");
        MapGraph instance = MapGraph.getInstance();
        instance.clearMapGraph();
        instance.addCity(A);
        String expResult = "A";
        String result = null;
        Iterator<City> it = instance.getCities();
        if (it != null) {
            if (it.hasNext()) {
                result = it.next().getId().trim();
            }
        }
        assertEquals(expResult, result);
    }

    /**
     * Test of findBestRoute method, of class MapGraph.
     */
    @Test
    public void testFindBestRoute() {
        System.out.println("findBestRoute");
        MapGraph instance = MapGraph.getInstance();
        instance.addCity(A);
        instance.addCity(B);
        instance.addCity(C);
        instance.addCity(D);
        double fuelPrice = 2.0;
        double autonomy = 10.0;
        double expResult = 12.0;
        MapGraph.Route result = instance.findBestRoute(A, D, fuelPrice, autonomy);
        assertEquals(expResult, result.getFuelCost(), 0.1);
        instance.clearMapGraph();
        instance.addCity(A1);
        instance.addCity(B1);
        instance.addCity(C1);
        instance.addCity(D1);
        instance.addCity(E1);
        instance.addCity(E1);
        instance.addCity(F1);
        result = instance.findBestRoute(A1, F1, fuelPrice, autonomy);
        String strResult = "City A1 - City B1; City B1 - City F1; fuel cost = 5.0";
        assertEquals(strResult, result.toString());
        Road ABx = new Road(1, 10.0, A, B);
        Road ACx = new Road(2, 20.0, A, C);
        Road BDx = new Road(3, 15.0, B, D);
        Road BEx = new Road(4, 50.0, B, E);
        Road CDx = new Road(5, 30.0, C, D);
        Road DEx = new Road(6, 30.0, D, E);
        A.clearAdjacents();
        B.clearAdjacents();
        C.clearAdjacents();
        D.clearAdjacents();
        E.clearAdjacents();
        F.clearAdjacents();
        A.addAdjacent(ABx);
        A.addAdjacent(ACx);
        B.addAdjacent(BDx);
        B.addAdjacent(BEx);
        C.addAdjacent(CDx);
        D.addAdjacent(DEx);
        instance.clearMapGraph();
        instance.addCity(A);
        instance.addCity(B);
        instance.addCity(C);
        instance.addCity(D);
        instance.addCity(E);
        instance.addCity(F);
        fuelPrice = 2.5;
        result = instance.findBestRoute(A, D, fuelPrice, autonomy);
        strResult = "City A - City B; City B - City D; fuel cost = 6.25";
        assertEquals(strResult, result.toString());
    }
    
}
