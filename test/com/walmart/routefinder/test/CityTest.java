/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.routefinder.test;

import com.walmart.routefinder.domain.City;
import com.walmart.routefinder.domain.Road;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Artur
 */
public class CityTest {
    
    public CityTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class City.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        City instance = new City();
        String expResult = "";
        String result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class City.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        String id = "";
        City instance = new City();
        instance.setId(id);
    }

    /**
     * Test of getName method, of class City.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        City instance = new City();
        String expResult = "";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setName method, of class City.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "";
        City instance = new City();
        instance.setName(name);
    }

    /**
     * Test of getDestinations method, of class City.
     */
    @Test
    public void testGetDestinations() {
        System.out.println("getRoadCollection");
        City instance = new City();
        Collection<Road> expResult = null;
        Collection<Road> result = instance.getDestinations();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDestinations method, of class City.
     */
    @Test
    public void testSetDestinations() {
        System.out.println("setRoadCollection");
        Collection<Road> roadCollection = null;
        City instance = new City();
        instance.setDestinations(roadCollection);
    }

    /**
     * Test of getOrigins method, of class City.
     */
    @Test
    public void testGetOrigins() {
        System.out.println("getRoadCollection1");
        City instance = new City();
        Collection<Road> expResult = null;
        Collection<Road> result = instance.getOrigins();
        assertEquals(expResult, result);
    }

    /**
     * Test of setOrigins method, of class City.
     */
    @Test
    public void testSetOrigins() {
        System.out.println("setRoadCollection1");
        Collection<Road> roadCollection1 = null;
        City instance = new City();
        instance.setOrigins(roadCollection1);
    }

    /**
     * Test of hashCode method, of class City.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        City instance = new City();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class City.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        City instance = new City();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class City.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        City instance = new City();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
