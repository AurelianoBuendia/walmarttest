/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.routefinder.test;

import com.walmart.routefinder.service.Route;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Artur
 */
public class RouteTest {
    
    public RouteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findBestRoute method, of class Route.
     */
    @Test
    public void testFindBestRoute() {
        System.out.println("findBestRoute");
        String originId = "A";
        String destinationId = "D";
        double fuelPrice = 2.5;
        double autonomy = 10.0;
        Route instance = new Route();
        String expResult = "City A - City B; City B - City D; fuel cost = 6.25";
        String result = instance.findBestRoute(originId, destinationId, fuelPrice, autonomy);
        assertEquals(expResult, result);
    }
    
}
