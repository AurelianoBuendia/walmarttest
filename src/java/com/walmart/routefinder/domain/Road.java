/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.routefinder.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "ROAD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Road.findAll", query = "SELECT r FROM Road r"),
    @NamedQuery(name = "Road.findById", query = "SELECT r FROM Road r WHERE r.id = :id"),
    @NamedQuery(name = "Road.findByDistance", query = "SELECT r FROM Road r WHERE r.distance = :distance")})
public class Road implements Serializable, Comparable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DISTANCE")
    private double distance;
    @JoinColumn(name = "DESTINATIONID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private City destinationid;
    @JoinColumn(name = "ORIGINID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private City originid;

    public Road() {
    }

    public Road(Integer id) {
        this.id = id;
    }

    public Road(Integer id, double distance) {
        this.id = id;
        this.distance = distance;
    }
    
    public Road(Integer id, double distance, City origin, City destination) {
        this.id = id;
        this.distance = distance;
        this.originid = origin;
        this.destinationid = destination;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public City getDestinationid() {
        return destinationid;
    }

    public void setDestinationid(City destinationid) {
        this.destinationid = destinationid;
    }

    public City getOriginid() {
        return originid;
    }

    public void setOriginid(City originid) {
        this.originid = originid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Road)) {
            return false;
        }
        Road other = (Road) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.walmart.routefinder.domain.Road[ id=" + id + " ]";
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Road) {
            Road otherRoad = (Road)o;
            return ((Double)this.distance).compareTo((Double)otherRoad.distance);
        }
        return -1;
    }
    
}
