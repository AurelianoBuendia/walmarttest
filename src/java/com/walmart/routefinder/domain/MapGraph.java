/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.routefinder.domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

/**
 *
 * @author Artur
 */
public class MapGraph {
    private static MapGraph instance;
    private HashSet<City> cities;
    private boolean isDirected = false;
    private final String url = "jdbc:derby://localhost:1527/WalmartTestDB";
    private final String userName = "walmart";
    private final String password = "walmart";
  
    public static synchronized MapGraph getInstance() {
        if (instance == null) {
            instance = new MapGraph();
            instance.cities = new HashSet<>();
            instance.readAllCities();
        }
        return instance;
    }
    
    public boolean getIsDirected() {
        return isDirected;
    }
    
    public void clearMapGraph() {
        if (cities != null) {
            cities.clear();
            return;
        }
        cities = new HashSet<>();
    }
    
    public void addCity(City city) {
        if (!cities.contains(city)) {
            cities.add(city);
        }
    }
    
    public City findCity(City city) {
        if (cities.contains(city)) {
            return city;
        }
        return null;
    }
    
    public City findCity(String id) {
        if (id != null) {
            Iterator<City> it = cities.iterator();
            while (it.hasNext()) {
                City city = it.next();
                if (city.getId().trim().equals(id.trim())) {
                    return city;
                }
            }
        }
        return null;
    }
    
    public Iterator<City> getCities() {
        if (cities != null) {
            return cities.iterator();
        }
        return null;
    }
    
    public void addRoad(Road road) {
        City origin = findCity(road.getOriginid());
        City destination = findCity(road.getDestinationid());
        if ((origin != null) && (destination != null)) {
            origin.addAdjacent(road);
            if (!isDirected) {
                destination.addAdjacent(new Road(road.getId(), 
                        road.getDistance(), destination, origin));
            }
        }
    }
                        
    public Route findBestRoute(final City origin, final City destination, 
            final double fuelPrice, final double autonomy) {
        LinkedList<Road> bestPath = searchShortestPath(origin, destination);
        Route result = new Route(origin, destination, bestPath, fuelPrice, autonomy);
        return result;
    }

    private LinkedList<Road> searchShortestPath(City city, City destination) {
        Stack<City> toVisit = new Stack();
        HashSet<City> visited = new HashSet<>();
        HashSet<Road> processed = new HashSet<>();
        LinkedList<Road> bestPath = new LinkedList();
        double length = Double.MAX_VALUE;
        LinkedList<Road> path = searchPath(city, destination, 
                toVisit, visited, processed);
        while (!path.isEmpty()) {
            Road lastRoad = path.getLast();
            if (lastRoad.getDestinationid().getId().trim().equals(destination.getId().trim())) {
                Iterator<Road> iter = path.iterator();
                double dist = 0.0;
                while (iter.hasNext()) {
                    Road curr = iter.next();
                    dist += curr.getDistance();
                }
                if (length > dist) {
                    length = dist;
                    bestPath = path;
                }
            }
            path = searchPath(city, destination, 
                toVisit, visited, processed);
        }
        return bestPath;
    }
   
    private LinkedList<Road> searchPath(City city, City destination,
            Stack<City> toVisit,
            HashSet<City> visited,
            HashSet<Road> processed) {
        LinkedList<Road> path = new LinkedList();
        toVisit.push(city);
        while (!toVisit.isEmpty()) {
            City current = toVisit.pop();
            if (current != null) {
                Iterator<Road> it = current.getAdjacents();
                boolean allRoadsProcessed = true;
                if (it != null) {
                    while (it.hasNext()) {
                        Road road = it.next();
                        if (!processed.contains(road)) {
                            allRoadsProcessed = false;
                            processed.add(road);
                            if (road.getDestinationid().getId().trim().equals(destination.getId().trim())) {
                                path.add(road);
                                break;
                            }
                            if (!visited.contains(road.getDestinationid())) {
                                toVisit.push(road.getDestinationid());
                                path.add(road);
                                break;
                            }
                        }
                    }
                    if (allRoadsProcessed) {
                        visited.add(current);
                    }
                }
            }
        }
        return path;
    }
    
    private void readAllCities() {
        readAllCitiesFromDatabase();
        try {
            Connection conn = DriverManager.getConnection(url, userName, password);
            Statement st = conn.createStatement();
            Iterator<City> it = cities.iterator();
            while (it.hasNext()) {
                City city = it.next();
                String query = "SELECT * FROM ROAD WHERE ORIGINID = '" + city.getId().trim() + "'";
                try (ResultSet roadsQry = st.executeQuery(query)) {
                    while (roadsQry.next()) {
                        int id = roadsQry.getInt("ID");
                        String destinationId = roadsQry.getString("DESTINATIONID");
                        double distance = roadsQry.getDouble("DISTANCE");
                        City destination = findCity(destinationId);
                        if (destination != null) {
                            Road road = new Road(id, distance, city, destination);
                            city.addAdjacent(road);
                        }
                    }
                }
            }
        } catch (SQLException e) {

        }
    }
    
    private void readAllCitiesFromDatabase() {
        try {
            cities.clear();
            Connection conn = DriverManager.getConnection(url, userName, password);
            Statement st = conn.createStatement();
            try (ResultSet citiesQry = st.executeQuery("SELECT * FROM CITY")) {
                while (citiesQry.next()) {
                    String id = citiesQry.getString(1).trim();
                    String name = citiesQry.getString(2).trim();
                    City city = new City(id, name);
                    cities.add(city);
                }
            }
        } catch (SQLException e) {
        }       
    }

    private City readCityFromDatabase(String id) {
        try {
            Connection conn = DriverManager.getConnection(url, userName, password);
            Statement st = conn.createStatement();
            String query = "SELECT * FROM CITY WHERE ID = '" + id.trim() + "'";
            try (ResultSet citiesQry = st.executeQuery(query)) {
                if (citiesQry.next()) {
                    String name = citiesQry.getString(2);
                    City city = new City(id.trim(), name);
                    return city;
                }
                return null;
            }
        } catch (SQLException e) {
        }       
        return null;
    }
    
    public class Route {
        private City origin;
        private City destination;
        private LinkedList<Road> route;
        private double distance;
        private double fuelPrice;
        private double autonomy;
        private double fuelCost;

        public City getOrigin() {
            return origin;
        }

        public City getDestination() {
            return destination;
        }

        public LinkedList<Road> getRoute() {
            return route;
        }

        public double getDistance() {
            return distance;
        }

        public double getFuelPrice() {
            return fuelPrice;
        }

        public double getAutonomy() {
            return autonomy;
        }

        public double getFuelCost() {
            return fuelCost;
        }
        
        public Route(City origin, City destination, LinkedList<Road> route, double fuelPrice, double autonomy) {
            this.origin = origin;
            this.destination = destination;
            this.route = route;
            this.fuelPrice = fuelPrice;
            this.autonomy = autonomy;
            this.distance = Double.POSITIVE_INFINITY;
            this.fuelCost = Double.POSITIVE_INFINITY;
            if (route != null) {
                this.distance = 0.0;
                Iterator<Road> it = route.iterator();
                while (it.hasNext()) {
                    this.distance += it.next().getDistance();
                }
                if (autonomy != 0) {
                    this.fuelCost = (distance / autonomy) * fuelPrice;
                }
            }
        }
        
        @Override
        public String toString() {
            if (route != null) {
                StringBuilder result = new StringBuilder();
                Iterator<Road> it = route.iterator();
                while (it.hasNext()) {
                    Road road = it.next();
                    result.append(road.getOriginid().getName());
                    result.append(" - ");
                    result.append(road.getDestinationid().getName());
                    result.append("; ");
                }
                result.append("fuel cost = ");
                result.append(this.fuelCost);
                return result.toString();
            }
            return "";
        }
    }
}
