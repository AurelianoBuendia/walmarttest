/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.routefinder.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.TreeSet;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "CITY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "City.findAll", query = "SELECT c FROM City c"),
    @NamedQuery(name = "City.findById", query = "SELECT c FROM City c WHERE c.id = :id"),
    @NamedQuery(name = "City.findByName", query = "SELECT c FROM City c WHERE c.name = :name")})
public class City implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "ID")
    private String id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NAME")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "destinationid")
    private Collection<Road> destinations;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "originid")
    private Collection<Road> origins = new TreeSet();
    //private Collection<Road> adjacents = new TreeSet<>();

    public City() {
    }

    public City(String id) {
        this.id = id.trim();
    }

    public City(String id, String name) {
        this.id = id.trim();
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<Road> getDestinations() {
        return destinations;
    }

    public void setDestinations(Collection<Road> destinations) {
        this.destinations = destinations;
    }

    @XmlTransient
    public Collection<Road> getOrigins() {
        return origins;
    }

    public void setOrigins(Collection<Road> origins) {
        this.origins = origins;
    }
    
    public Iterator<Road> getAdjacents() {
        if (this.origins != null) {
            return this.origins.iterator();
        }
        return null;
    }
    
    public boolean addAdjacent(Road road) {
        if (this.origins != null) {
            return this.origins.add(road);
        }
        return false;
    }
    
    public boolean removeAdjacent(Road road) {
        if (this.origins != null) {
            return this.origins.remove(road);
        }
        return false;
    }
    
    public void clearAdjacents() {
        this.origins.clear();
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof City)) {
            return false;
        }
        City other = (City) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.walmart.routefinder.domain.City[ id=" + id + " ]";
    }
    
}
