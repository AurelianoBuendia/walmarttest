/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.routefinder.service;

import com.walmart.routefinder.domain.City;
import com.walmart.routefinder.domain.MapGraph;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ws.rs.Produces;

/**
 *
 * @author Artur
 */
@WebService(serviceName = "Route")
public class Route {

    @WebMethod(operationName = "findBestRoute")
    @Produces({"text/plain"})
    public String findBestRoute(@WebParam(name = "origin") final String originId, 
            @WebParam(name = "destination") final String destinationId, 
            @WebParam(name = "fuelprice") final double fuelPrice, 
            @WebParam(name = "autonomy") final double autonomy) {
        MapGraph instance = MapGraph.getInstance();
        City origin = instance.findCity(originId);
        City destination = instance.findCity(destinationId);
        if ((origin != null) && (destination != null)) {
            MapGraph.Route route = instance.findBestRoute(origin, destination,
                    fuelPrice, autonomy);
            return route.toString();
        }
        return "NA";
    }
    
    
}
