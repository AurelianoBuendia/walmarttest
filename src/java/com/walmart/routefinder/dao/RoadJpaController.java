/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.routefinder.dao;

import com.walmart.routefinder.dao.exceptions.NonexistentEntityException;
import com.walmart.routefinder.dao.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.walmart.routefinder.domain.City;
import com.walmart.routefinder.domain.Road;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author Artur
 */
public class RoadJpaController implements Serializable {

    public RoadJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Road road) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            City destinationid = road.getDestinationid();
            if (destinationid != null) {
                destinationid = em.getReference(destinationid.getClass(), destinationid.getId());
                road.setDestinationid(destinationid);
            }
            City originid = road.getOriginid();
            if (originid != null) {
                originid = em.getReference(originid.getClass(), originid.getId());
                road.setOriginid(originid);
            }
            em.persist(road);
            if (destinationid != null) {
                destinationid.getDestinations().add(road);
                destinationid = em.merge(destinationid);
            }
            if (originid != null) {
                originid.getDestinations().add(road);
                originid = em.merge(originid);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Road road) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Road persistentRoad = em.find(Road.class, road.getId());
            City destinationidOld = persistentRoad.getDestinationid();
            City destinationidNew = road.getDestinationid();
            City originidOld = persistentRoad.getOriginid();
            City originidNew = road.getOriginid();
            if (destinationidNew != null) {
                destinationidNew = em.getReference(destinationidNew.getClass(), destinationidNew.getId());
                road.setDestinationid(destinationidNew);
            }
            if (originidNew != null) {
                originidNew = em.getReference(originidNew.getClass(), originidNew.getId());
                road.setOriginid(originidNew);
            }
            road = em.merge(road);
            if (destinationidOld != null && !destinationidOld.equals(destinationidNew)) {
                destinationidOld.getDestinations().remove(road);
                destinationidOld = em.merge(destinationidOld);
            }
            if (destinationidNew != null && !destinationidNew.equals(destinationidOld)) {
                destinationidNew.getDestinations().add(road);
                destinationidNew = em.merge(destinationidNew);
            }
            if (originidOld != null && !originidOld.equals(originidNew)) {
                originidOld.getDestinations().remove(road);
                originidOld = em.merge(originidOld);
            }
            if (originidNew != null && !originidNew.equals(originidOld)) {
                originidNew.getDestinations().add(road);
                originidNew = em.merge(originidNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = road.getId();
                if (findRoad(id) == null) {
                    throw new NonexistentEntityException("The road with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Road road;
            try {
                road = em.getReference(Road.class, id);
                road.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The road with id " + id + " no longer exists.", enfe);
            }
            City destinationid = road.getDestinationid();
            if (destinationid != null) {
                destinationid.getDestinations().remove(road);
                destinationid = em.merge(destinationid);
            }
            City originid = road.getOriginid();
            if (originid != null) {
                originid.getDestinations().remove(road);
                originid = em.merge(originid);
            }
            em.remove(road);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Road> findRoadEntities() {
        return findRoadEntities(true, -1, -1);
    }

    public List<Road> findRoadEntities(int maxResults, int firstResult) {
        return findRoadEntities(false, maxResults, firstResult);
    }

    private List<Road> findRoadEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Road.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Road findRoad(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Road.class, id);
        } finally {
            em.close();
        }
    }

    public int getRoadCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Road> rt = cq.from(Road.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
