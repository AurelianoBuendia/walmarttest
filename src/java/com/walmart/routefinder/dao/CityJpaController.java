/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.walmart.routefinder.dao;

import com.walmart.routefinder.dao.exceptions.IllegalOrphanException;
import com.walmart.routefinder.dao.exceptions.NonexistentEntityException;
import com.walmart.routefinder.dao.exceptions.PreexistingEntityException;
import com.walmart.routefinder.dao.exceptions.RollbackFailureException;
import com.walmart.routefinder.domain.City;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.walmart.routefinder.domain.Road;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author Artur
 */
public class CityJpaController implements Serializable {

    public CityJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(City city) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (city.getDestinations() == null) {
            city.setDestinations(new ArrayList<Road>());
        }
        if (city.getOrigins() == null) {
            city.setOrigins(new ArrayList<Road>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<Road> attachedDestinations = new ArrayList<Road>();
            for (Road destinationsRoadToAttach : city.getDestinations()) {
                destinationsRoadToAttach = em.getReference(destinationsRoadToAttach.getClass(), destinationsRoadToAttach.getId());
                attachedDestinations.add(destinationsRoadToAttach);
            }
            city.setDestinations(attachedDestinations);
            Collection<Road> attachedOrigins = new ArrayList<Road>();
            for (Road originsRoadToAttach : city.getOrigins()) {
                originsRoadToAttach = em.getReference(originsRoadToAttach.getClass(), originsRoadToAttach.getId());
                attachedOrigins.add(originsRoadToAttach);
            }
            city.setOrigins(attachedOrigins);
            em.persist(city);
            for (Road destinationsRoad : city.getDestinations()) {
                City oldDestinationidOfDestinationsRoad = destinationsRoad.getDestinationid();
                destinationsRoad.setDestinationid(city);
                destinationsRoad = em.merge(destinationsRoad);
                if (oldDestinationidOfDestinationsRoad != null) {
                    oldDestinationidOfDestinationsRoad.getDestinations().remove(destinationsRoad);
                    oldDestinationidOfDestinationsRoad = em.merge(oldDestinationidOfDestinationsRoad);
                }
            }
            for (Road originsRoad : city.getOrigins()) {
                City oldOriginidOfOriginsRoad = originsRoad.getOriginid();
                originsRoad.setOriginid(city);
                originsRoad = em.merge(originsRoad);
                if (oldOriginidOfOriginsRoad != null) {
                    oldOriginidOfOriginsRoad.getOrigins().remove(originsRoad);
                    oldOriginidOfOriginsRoad = em.merge(oldOriginidOfOriginsRoad);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findCity(city.getId()) != null) {
                throw new PreexistingEntityException("City " + city + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(City city) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            City persistentCity = em.find(City.class, city.getId());
            Collection<Road> destinationsOld = persistentCity.getDestinations();
            Collection<Road> destinationsNew = city.getDestinations();
            Collection<Road> originsOld = persistentCity.getOrigins();
            Collection<Road> originsNew = city.getOrigins();
            List<String> illegalOrphanMessages = null;
            for (Road destinationsOldRoad : destinationsOld) {
                if (!destinationsNew.contains(destinationsOldRoad)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Road " + destinationsOldRoad + " since its destinationid field is not nullable.");
                }
            }
            for (Road originsOldRoad : originsOld) {
                if (!originsNew.contains(originsOldRoad)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Road " + originsOldRoad + " since its originid field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Road> attachedDestinationsNew = new ArrayList<Road>();
            for (Road destinationsNewRoadToAttach : destinationsNew) {
                destinationsNewRoadToAttach = em.getReference(destinationsNewRoadToAttach.getClass(), destinationsNewRoadToAttach.getId());
                attachedDestinationsNew.add(destinationsNewRoadToAttach);
            }
            destinationsNew = attachedDestinationsNew;
            city.setDestinations(destinationsNew);
            Collection<Road> attachedOriginsNew = new ArrayList<Road>();
            for (Road originsNewRoadToAttach : originsNew) {
                originsNewRoadToAttach = em.getReference(originsNewRoadToAttach.getClass(), originsNewRoadToAttach.getId());
                attachedOriginsNew.add(originsNewRoadToAttach);
            }
            originsNew = attachedOriginsNew;
            city.setOrigins(originsNew);
            city = em.merge(city);
            for (Road destinationsNewRoad : destinationsNew) {
                if (!destinationsOld.contains(destinationsNewRoad)) {
                    City oldDestinationidOfDestinationsNewRoad = destinationsNewRoad.getDestinationid();
                    destinationsNewRoad.setDestinationid(city);
                    destinationsNewRoad = em.merge(destinationsNewRoad);
                    if (oldDestinationidOfDestinationsNewRoad != null && !oldDestinationidOfDestinationsNewRoad.equals(city)) {
                        oldDestinationidOfDestinationsNewRoad.getDestinations().remove(destinationsNewRoad);
                        oldDestinationidOfDestinationsNewRoad = em.merge(oldDestinationidOfDestinationsNewRoad);
                    }
                }
            }
            for (Road originsNewRoad : originsNew) {
                if (!originsOld.contains(originsNewRoad)) {
                    City oldOriginidOfOriginsNewRoad = originsNewRoad.getOriginid();
                    originsNewRoad.setOriginid(city);
                    originsNewRoad = em.merge(originsNewRoad);
                    if (oldOriginidOfOriginsNewRoad != null && !oldOriginidOfOriginsNewRoad.equals(city)) {
                        oldOriginidOfOriginsNewRoad.getOrigins().remove(originsNewRoad);
                        oldOriginidOfOriginsNewRoad = em.merge(oldOriginidOfOriginsNewRoad);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = city.getId();
                if (findCity(id) == null) {
                    throw new NonexistentEntityException("The city with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            City city;
            try {
                city = em.getReference(City.class, id);
                city.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The city with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Road> destinationsOrphanCheck = city.getDestinations();
            for (Road destinationsOrphanCheckRoad : destinationsOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This City (" + city + ") cannot be destroyed since the Road " + destinationsOrphanCheckRoad + " in its destinations field has a non-nullable destinationid field.");
            }
            Collection<Road> originsOrphanCheck = city.getOrigins();
            for (Road originsOrphanCheckRoad : originsOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This City (" + city + ") cannot be destroyed since the Road " + originsOrphanCheckRoad + " in its origins field has a non-nullable originid field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(city);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<City> findCityEntities() {
        return findCityEntities(true, -1, -1);
    }

    public List<City> findCityEntities(int maxResults, int firstResult) {
        return findCityEntities(false, maxResults, firstResult);
    }

    private List<City> findCityEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(City.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public City findCity(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(City.class, id);
        } finally {
            em.close();
        }
    }

    public int getCityCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<City> rt = cq.from(City.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
